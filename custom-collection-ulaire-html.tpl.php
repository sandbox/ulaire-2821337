<?php

/**
 * @file
 * Default implementation of the custom-collection-ulaire-html template.
 *
 * Available variables:
 * - $title_tag
 * - $active_menu_item
 * - $content
 *
 */

?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><?php print $title_tag; ?></title>
<link rel="shortcut icon" href="http://ulaire.webs.com/favicon.png">
<link rel="canonical" href="http://ulaire.webs.com/list.html">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=UnifrakturCook:700">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Economica">
<link rel="stylesheet" href="http://ulaire.webs.com/js/fancybox/jquery.fancybox.css?v=2.0.6">
<link rel="stylesheet" href="http://ulaire.webs.com/css/ulaire.css">
<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-33098565-1']);
_gaq.push(['_trackPageview']);
(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>
</head>
<body>
	<div id="wrapper">
		<header>
			<nav>
				<ul>
					<li><a href="http://ulaire.webs.com/">Home</a></li>
					<li><a href="http://ulaire.webs.com/list.html"<?php if ($active_menu_item == 'audio'): ?> class="active"<?php endif; ?>>Audio</a></li>
					<li><a href="http://ulaire.webs.com/video.html"<?php if ($active_menu_item == 'video'): ?> class="active"<?php endif; ?>>Video</a></li>
					<li><a href="http://ulaire.webs.com/photos/">Photos</a></li>
					<li><a href="http://ulaire.webs.com/contact.html">Contact</a></li>
				</ul>
			</nav>
		</header>
		<div id="main">
			<div id="sidebar-first"></div>
			<div id="content">
				<?php print $content; ?>
			</div>
			<div id="sidebar-second"></div>
		</div>
		<footer>
			copyright ulaire 2012 | <a href="http://ulaire.webs.com/contact.html">contact</a>
		</footer>
	</div>
<script src="http://images.freewebs.com/JS/Freebar/bar_stretchblack.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script src="http://ulaire.webs.com/js/fancybox/jquery.fancybox.pack.js?v=2.0.6"></script>
<script src="http://ulaire.webs.com/js/ulaire.js"></script>
</body>
</html>
