<?php

/**
 * @file
 * Callback functions and forms used to display page content.
 */


/**
 * Page callback for the music bands overview_page.
 */
function _custom_collection_music_bands_overview_page() {
  include_once DRUPAL_ROOT . '/includes/locale.inc';
  // get the bands
  $query = db_select('music_bands', 'mb');
  $query->fields('mb');
  $query->orderBy('mb.name');
  $list = $query->execute()->fetchAll(PDO::FETCH_ASSOC);
  // get the countries
  $countries = country_get_list();
  // build table
  $header = array(
    'name' => array('data' => t('name')),
    'country' => array('data' => t('country')),
    'show_country' => array('data' => t('show country')),
    'info' => array('data' => t('info')),
    'details' => array('data' => ''),
  );
  $items['header'] = $header;
  $items['rows'] = array();
  $items['empty'] = t('No releases found.');
  foreach ($list as $band) {
    $items['rows'][] = array(
      'data' => array(
        array('data' => $band['name']),
        array('data' => (!empty($countries[$band['country']])) ? $countries[$band['country']] : ''),
        array('data' => ($band['show_country']) ? $band['show_country'] : ''),
        array('data' => ($band['info']) ? $band['info'] : ''),
        array('data' => l(t('edit'), 'admin/custom-collection/music/band/' . $band['id'] . '/edit', array('query' => array('destination' => 'admin/custom-collection/music/bands#band-' . $band['id'])))),
      ),
      'id' => 'band-' . $band['id'],
    );
  }
  // return content
  $content = '<p><b>total:</b> ' . count($list) . '</p>';
  $items['attributes'] = array('style' => 'width:1000px;');
  $content .= theme('table', $items);
  return $content;
}

/**
 * Page callback to display the audio list in html format.
 */
function _custom_collection_music_export_audio_page() {
  // list
  $list_markup = '<p>';
  $list = _custom_collection_get_music_list('audio');
  $count = 0;
  $previous_first_character = '';
  foreach ($list as $entry) {
    if ($entry['site']) {
      $first_character = $entry['first_character'];
      if ($count && $first_character != $previous_first_character) {
        $list_markup .= "</p>\n<p>";
      }
      $list_markup .= $entry['line'] . "<br>\n";
      $count++;
      $previous_first_character = $first_character;
    }
  }
  $list_markup .= '</p><p><b>' . $count . ' TITELS</b></p>';
  // content
  $content = '<h1>Audio Collection</h1>';
  $content .= '<img class="content-image" src="http://ulaire.webs.com/images/watain.jpg" alt="cult black metal forever">';
  $content .= '<p>This is my complete music collection list. As stated before, nothing here is for sale. This list is only meant to trade dubs of rare demo, rehearsal and live recordings. If there\'s anything on here that interests you, feel free to <a href="http://ulaire.webs.com/contact.html">contact me</a>, I\'m sure we can work something out.</p>';
  $content .= '<fieldset>';
  $content .= ' <legend>Info</legend>';
  $content .= ' <strong>BOLD = original</strong><br>';
  $content .= ' GREY = dub<br>';
  $content .= ' * = dub with copy of the democover';
  $content .= '</fieldset>';
  $content .= '<p><strong>LAST UPDATED: ' . date('d/m/Y') . '</strong></p>';
  $content .= '<div id="list">' . $list_markup . '</div>';
  print theme('custom_collection_ulaire_html', array('title_tag' => 'ulaire | audio list for tape and cd-r trade', 'active_menu_item' => 'audio', 'content' =>$content));
}

/**
 * Page callback to display the video list in html format.
 */
function _custom_collection_music_export_video_page() {
  $content = '<h1>Video Collection</h1>';
  $content .= '<img class="content-image" src="http://ulaire.webs.com/images/antaeus.jpg" alt="cult black metal forever">';
  $content .= '<p>I mostly trade video on through Soulseek these days. I\'ve pretty much had it with burning dvd\'s and going to the post office (and pay absurd prices to send a package). Soulseek is just so much better for trading video. So please <a href="http://ulaire.webs.com/contact.html">contact me</a> and we\'ll work something out.</p>';
  $content .= '<p>Almost all these shows are dvd quality. A lot of them were ripped from vhs by myself. There are still a few in mpg format (I really need to remove those), but I\'ll let you know if you picked one of those.</p>';
  $content .= '<p>For the ones among you who are only interested in the audio of these live shows,<br />I can transfer both dvd and vhs to cd-r.</p>';
  $content .= '<p><strong>LAST UPDATED: ' . date('d/m/Y') . '</strong></p>';
  $content .= '<div id="list">';
  // get the list
  $list = _custom_collection_get_music_list('video');
  // recordings non-vhs
  $content .= '<h2>SHOWS ON DVD</h2>';
  $content .= '<p>';
  $count = 0;
  $previous_first_character = '';
  foreach ($list as $entry) {
    if ($entry['type'] == 'recording' && $entry['format'] != 'vhs' && $entry['site']) {
      $first_character = $entry['first_character'];
      if ($count && $first_character != $previous_first_character) {
        $content .= "</p>\n<p>";
      }
      $content .= $entry['line'] . "<br>\n";
      $count++;
      $previous_first_character = $first_character;
    }
  }
  $content .= '</p>';
  // recordings vhs
  $content .= '<h2>SHOWS ON VHS (I will convert these to dvd one day, I promise...)</h2>';
  $content .= '<p>';
  foreach ($list as $entry) {
    if ($entry['type'] == 'recording' && $entry['format'] == 'vhs' && $entry['site']) {
      $content .= $entry['line'] . "<br>\n";
    }
  }
  $content .= '</p>';
  // releases
  $content .= '<h2>OFFICIAL</h2>';
  foreach ($list as $entry) {
    if ($entry['type'] == 'release' && $entry['site']) {
      $content .= $entry['line'] . "<br>\n";
    }
  }
  $content .= '</div>';
  print theme('custom_collection_ulaire_html', array('title_tag' => 'ulaire | audio list for tape and cd-r trade', 'active_menu_item' => 'video', 'content' =>$content));
}

/**
 * Form API callback for the music band detail form.
 */
function _custom_collection_music_band_form($form, &$form_state, $band_id = NULL) {
  if ($band_id) {
    // get band info
    $band = db_select('music_bands', 'mb')->fields('mb')->condition('id', $band_id)->execute()->fetchAssoc();
    // get releases/recordings
    $list = _custom_collection_get_music_list('all', $band_id);
  }
  $form['band_id'] = array(
    '#type' => 'value',
    '#value' => $band_id,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => isset($band['name']) ? $band['name'] : '',
  );
  $form['country'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#options' => country_get_list(),
    '#empty_option' => t('- None -'),
    '#default_value' => isset($band['country']) ? $band['country'] : '',
  );
  $form['show_country'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show country'),
    '#default_value' => isset($band['show_country']) ? $band['show_country'] : 0,
  );
  $form['info'] = array(
    '#type' => 'textfield',
    '#title' => t('Info'),
    '#default_value' => isset($band['info']) ? $band['info'] : '',
  );
  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  if ($band_id) {
    $form['actions']['delete'] = array(
      '#type' => 'link',
      '#title' => t('Delete'),
      '#href' => 'admin/custom-collection/music/band/' . $band_id . '/delete',
    );
    $rows = array();
    foreach ($list as $entry) {
      $rows[] = array(
        'data' => array(
          array('data' => $entry['line']),
          array('data' => l(t('edit'), 'admin/custom-collection/music/' . $entry['type'] . '/' . $entry['id'] . '/edit', array('query' => array('destination' => 'admin/custom)collection/music#' . $entry['type'] . '-' . $entry['id'] . '-' . $entry['band_id'])))),
        ),
      );
    }
    $form['collection'] = array(
      '#type' => 'fieldset',
      '#title' => t('Collection'),
    );
    $form['collection']['list'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
    );
  }
  return $form;
}

/**
 * Form API submit callback for the music band detail form.
 */
function _custom_collection_music_band_form_submit($form, &$form_state) {
  // collect band data
  $band = array(
    'id' => NULL,
    'name' => $form_state['values']['name'],
    'country' => $form_state['values']['country'],
    'show_country' => $form_state['values']['show_country'],
    'info' => $form_state['values']['info'],
  );
  // check for update or insert
  if (empty($form_state['values']['band_id'])) {
    // insers
    drupal_write_record('music_bands', $band);
  } else {
    // add id
    $band['id'] = $form_state['values']['band_id'];
    // update
    drupal_write_record('music_bands', $band, 'id');
  }
  // set anchor if needed
  if (empty($_GET['destination'])) {
    drupal_goto('admin/custom-collection/music/bands', array('fragment' => 'band-' . $band['id']));
  }
}
