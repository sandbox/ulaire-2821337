<?php

/**
 * @file
 * Callback functions and forms used for the rode ridder collection.
 */


/**
 * Page callback for the music bands overview_page.
 */
function _custom_collection_roderidder_overview_page() {
  // get the albums
  $query = db_select('roderidder', 'rr');
  $query->fields('rr');
  $query->orderBy('rr.release_id');
  $list = $query->execute()->fetchAll(PDO::FETCH_ASSOC);
  // build table
  $header = array(
    'release_id' => array('data' => t('id')),
    'title' => array('data' => t('title')),
    'year' => array('data' => t('year')),
    'blue_and_brown' => array('data' => t('blue and brown')),
    'black_and_white' => array('data' => 'black and white'),
    'color' => array('data' => 'color'),
    'details' => array('data' => ''),
  );
  $items['header'] = $header;
  $items['rows'] = array();
  $items['empty'] = t('No albums found.');
  foreach ($list as $album) {
    $items['rows'][] = array(
      'data' => array(
        array('data' => $album['release_id']),
        array('data' => $album['title']),
        array('data' => $album['year']),
        array('data' => ($album['blue_and_brown']) ? 'v' : ''),
        array('data' => ($album['black_and_white']) ? 'v' : ''),
        array('data' => ($album['color']) ? 'v' : ''),
        array('data' => l(t('edit'), 'admin/custom-collection/roderidder/album/' . $album['release_id'] . '/edit', array('query' => array('destination' => 'admin/custom-collection/roderidder#album-' . $album['release_id'])))),
      ),
      'id' => 'album-' . $album['release_id'],
    );
  }
  // return content
  $content = theme('table', $items);
  return $content;
}

/**
 * Form API callback for the rode ridder album detail form.
 */
function _custom_collection_roderidder_album_form($form, &$form_state, $album_id = NULL) {
  if ($album_id) {
    // get album info
    $album = db_select('roderidder', 'rr')->fields('rr')->condition('release_id', $album_id)->execute()->fetchAssoc();
  }
  $form['album_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Album Number'),
    '#default_value' => $album_id,
    '#required' => TRUE,
    '#element_validate' => array('element_validate_integer_positive'),
  );
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => isset($album['title']) ? $album['title'] : '',
  );
  $form['year'] = array(
    '#type' => 'textfield',
    '#title' => t('Year'),
    '#default_value' => isset($album['year']) ? $album['year'] : '',
    '#required' => TRUE,
    '#element_validate' => array('element_validate_integer_positive'),
  );
  $form['blue_and_brown'] = array(
    '#type' => 'checkbox',
    '#title' => t('blue and brown'),
    '#default_value' => isset($album['blue_and_brown']) ? $album['blue_and_brown'] : 0,
  );
  $form['black_and_white'] = array(
    '#type' => 'checkbox',
    '#title' => t('black and white'),
    '#default_value' => isset($album['black_and_white']) ? $album['black_and_white'] : 0,
  );
  $form['color'] = array(
    '#type' => 'checkbox',
    '#title' => t('color'),
    '#default_value' => isset($album['color']) ? $album['color'] : 0,
  );
  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Form API submit callback for the rode ridder album detail form.
 */
function _custom_collection_roderidder_album_form_submit($form, &$form_state) {
  // collect album data
  $album = array(
    'release_id' => $form_state['values']['album_id'],
    'title' => $form_state['values']['title'],
    'year' => $form_state['values']['year'],
    'blue_and_brown' => $form_state['values']['blue_and_brown'],
    'black_and_white' => $form_state['values']['black_and_white'],
    'color' => $form_state['values']['color'],
  );
  // save
  db_merge('roderidder')->key(array('release_id' => $album['release_id']))->fields($album)->execute();
  // set anchor if needed
  if (empty($_GET['destination'])) {
    drupal_goto('admin/custom-collection/roderidder', array('fragment' => 'album-' . $album['release_id']));
  }
}

/**
 * Form API callback for the rode ridder album export form.
 */
function _custom_collection_roderidder_album_export_form($form, &$form_state) {
  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Full list as excel'),
  );
  return $form;
}

/**
 * Form API submit callback for the rode ridder album export form.
 */
function _custom_collection_roderidder_album_export_form_submit($form, &$form_state) {
  global $user;
  $excel = array(
    'headers' => array(),
    'ignore_headers' => TRUE,
    'data' => array(),
  );
  $query = db_select('roderidder', 'rr');
  $query->fields('rr');
  $entries = $query->execute()->fetchAll(PDO::FETCH_ASSOC);
  //$excel['headers']['roderidder'] = array_keys($entries[0]);
  foreach ($entries as $entry) {
    // @TODO: display mode of boolean values
    //$excel['data']['roderidder'][] = array_values($entry);
    $title = $entry['title'];
    $info = array();
    if ($entry['blue_and_brown']) {
      $info[] = 'bb';
    }
    if ($entry['black_and_white']) {
      $info[] = 'zw';
    }
    if ($entry['color']) {
      $info[] = 'kl';
    }
    if ($info) {
      $info = implode(', ', $info);
    } else {
      $info = '';
    }
    $excel['data']['roderidder'][] = array(
      $title,
      $info,
    );
  }
  // load phpexcel.inc from the phpexcel module.
  module_load_include('inc', 'phpexcel', 'phpexcel');
  // export to excel
  $export_file_name = 'roderidder-' . REQUEST_TIME . '.xls';
  $export_file_uri = 'public://' . $export_file_name;
  $export_file_path = drupal_realpath($export_file_uri);
  $options = array(
    'ignore_headers' => TRUE,
  );
  $status = phpexcel_export($excel['headers'], $excel['data'], $export_file_path, $options);
  if ($status == PHPEXCEL_SUCCESS) {
    // create a drupal file object.
    $file = new stdClass();
    $file->fid = NULL;
    $file->uri = $export_file_uri;
    $file->filename = drupal_basename($export_file_uri);
    $file->filemime = file_get_mimetype($export_file_uri);
    $file->uid = $user->uid;
    $file->status = 0; // temporary file
    // save the file and add it to the results
    $saved_file = file_save($file);
    // set message
    drupal_set_message(t('Export created: !export', array('!export' => l($saved_file->filename, file_create_url($saved_file->uri)))), 'status');
  }
}
